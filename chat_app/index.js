var express = require('express');
var socketIo = require('socket.io');
var http = require('http');

const app = express();
const server = http.createServer(app);
const io = socketIo.listen(server);
const UsersService = require('./UsersService');

const userService = new UsersService();

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
	console.log('user connected');
	socket.on('join', function(name){
		userService.addUser({
			id: socket.id, 
			name
		});
		io.emit('update', {
			users: userService.getAllUsers()
		})
	})

	socket.on('disconnect', () => {
		userService.removeUser(socket.id);
		socket.broadcast.emit('update', {
			users: userService.getAllUsers()
		});
	});

	socket.on('message', function(message){
		const {name} = userService.getUserById(socket.id);
		socket.broadcast.emit('message', {
			text: message.text,
			from: name
		});
	});
});

server.listen(8080, function(){
	console.log('listening on *:8080');
}) 

 