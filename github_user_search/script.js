class App extends React.Component {
  constructor() {
    super();
    this.state = {
      searchText: '',
      users: []
    };
  }

  onChangeHandle(event) {
    this.setState({searchText: event.target.value});
  }

  onSubmit(event) {
    event.preventDefault();
    const {searchText} = this.state;
    const url = `https://api.github.com/search/users?q=${searchText}`;
    fetch(url)
      .then(response => response.json())
      .then(responseJson => this.setState({users: responseJson.items}));
  }

  render() {
    return (
      <div className="main">
        <div className="wrapper">
          <form onSubmit={event => this.onSubmit(event)}>
            <input
              placeholder="Search by user name"
              type="text"
              id="searchText"
              onChange={event => this.onChangeHandle(event)}
              value={this.state.searchText}/>
          </form>
        </div>
        <UsersList users={this.state.users}/>
      </div>
    );
  }
}


class UsersList extends React.Component{
	get users() {
		return this.props.users.map(user => <User key={user.id} user={user}/>);

  }
  render() {
  	return (
  		<div className="userlist">{this.users}</div>
    );
  }
}

class User extends React.Component {
  render() {
    return (
      <div className="user">
        <div className="userphoto">
          <img src={this.props.user.avatar_url}  />
        </div>
        <div className="userinfo">
          <h3>{this.props.user.login}</h3>
        </div>
        <div className="userbutton">
          <a className="button" href={this.props.user.html_url} target="_blank">View Profile</a>
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);