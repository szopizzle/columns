import React  from 'react';
import './App.css';
import CommentsListContainer from './CommentsListContainer';	
import 'font-awesome/css/font-awesome.css'

const  App = () => {
    return (
      <div className="App">
         <CommentsListContainer />
      </div>
    );
  }

export default App;
