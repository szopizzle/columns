import React from 'react';
import 'font-awesome/css/font-awesome.css'

const Comment = ({text, votes, id, thumbUp, thumbDown, removeComment}) => 
	<li><h2>{text}</h2><h4>votes: {votes}</h4>
		<button onClick={()=> thumbUp(id)}><i className='fa fa-thumbs-o-up fa-2x' aria-hidden='true'></i></button>
		<button onClick={()=> thumbDown(id)}><i className='fa fa-thumbs-o-down fa-2x' aria-hidden='true'></i></button>
		<button onClick={()=> removeComment(id)}><i className='fa fa-remove fa-2x' aria-hidden='true'></i></button>
	</li>;

export default Comment;