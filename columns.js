function columnDivider(txt){
	
	var result = [], 
	lettersCount = 0, 
	row =[], 
	lineToAdd = '';
	rowWidth = 30, 
	text = txt.split(' ');
	
	if (text.length<60){
		console.log('text is too short, over  60 characters accepted')
		console.log('');
	} else {
		console.log(''); 
		for (i = 0; i < text.length; i++){
			row.push(text[i]);
			lettersCount += text[i].length + 1;
			if (lettersCount >= rowWidth || i === text.length - 1) {
				var lastItem = row.pop()
				lineToAdd = row.join(' ');
				if (i === text.length - 1){
					lettersCount+=lastItem.length;
					lineToAdd+=' ';
					lineToAdd += lastItem;
				}
				var lenDiff = rowWidth - lineToAdd.length;
				
				for (j = 0; j < lenDiff; j++){
					lineToAdd +=' ';
				}
				
				lettersCount = lastItem.length;				
				result.push(lineToAdd)
				row = [lastItem];
				lineToAdd = '';
			}
		}

		var secColStart = Math.floor(result.length/2), l=0;
		if (result.length % 2 == 0){
			while(l<secColStart){
				console.log(result[l]+ ' ' + result[l + secColStart])
				l++;
			}
		} else {
			while(l < secColStart){
				console.log(result[l]+ ' ' + result[l + secColStart +1 ])
				l++;
				if (l==secColStart){
					console.log(result[secColStart])
					break;
				}
			}
		}
	}

}

var toDivide = 'A, Twardowski witam bracie To mówiąc, bieży obcesem Cóż to czyliż mię nie znacie? Jestem Mefistofelesem. Wszak ze mnąś na Łysej Górze Robił o duszę zapisy Cyrograf na byczej skórze Podpisałeś ty, i bisy miały słuchać twego rymu Ty, jak dwa lata przebiegą, Miałeś pojechać do Rzymu, By cię tam porwać jak swego. Już i siedem lat uciekło, Cyrograf nadal nie służy Ty, czarami dręcząc piekło, Ani myślisz o podróży.';
var toDivideTwo = 'Litwo, Ojczyzno moja! ty jesteś jak zdrowie. Ile cię trzeba cenić, ten tylko się dowie, Kto cię stracił. Dziś piękność twą w całej ozdobie, Widzę i opisuję, bo tęsknię po tobie. Panno święta, co Jasnej bronisz Częstochowy I w Ostrej świecisz Bramie! Ty, co gród zamkowy Nowogródzki ochraniasz z jego wiernym ludem! Jak mnie dziecko do zdrowia powróciłaś cudem.';
var toDivideThree = 'Rad jestem niezmiernie, że przekładu tego dokonał autor do takiej pracy wyjątkowo uzdolniony, bo wykształcony na swojskiej i obcej filozofii, a nadto, jako wielce zasłużony pisarz na polu dziejów naszej literatury, wyrobiony wszechstronnie pod względem językowym. Oba te czynniki przyłożyły się też znakomicie do tego, że Piotra Chmielowskiego spolszczenie tak ciężkiego, a często nawet bardzo zawiłego dzieła, jakim jest Krytyka czystego rozumu, należy niewątpliwie do najlepszych wśród licznych przekładów tego dzieła na inne języki. Znam bliżej przekłady francuski Tissota (1835), angielskie: Haywooda (1838), Meiklejohna (1852) i Maxa Müllera (1881), oraz rosyjski Władisławlewa (1867), a przy wydaniu niniejszego, porównywając ze sobą trudniejsze ustępy tych przekładów, przekonałem się często, że polskiemu należy przyznać pierwszeństwo pod względem zręcznego połączenia wierności i ścisłości z jasnością i potoczystością wysłowienia.We wspomnianych przekładach ścisła zgodność z oryginałem pociągała za sobą zazwyczaj zwroty wymuszone, niejasne, — co powiedzieć należy nawet o przekładzie Maxa Müllera; — chęć zaś przystępniejszego wykładu rozumowań Kanta łączyła się często z zatarciem lub przynajmniej z osłabieniem subtelnych odcieni jego myśli, — jak o tym świadczą przekłady Tissota i Władisławlewa. Chmielowski natomiast w najzawilszych i najtrudniejszych miejscach znalazł zawsze szczęśliwe wyjście między tą Scyllą i Charybdą tłumaczów. Ścisłość i wierność jego przekładu nie ucierpiała wcale na tym, że dbał o możliwie jasny i przystępny wykład przez dobór wyrażeń i zwrotów dobrze zrozumiałych, a szczególniej przy pomocy rozbicia zbyt długich okresów na mniejsze, odpowiadające bardziej duchowi naszego języka.Rzecz naturalna, że i największa w tym kierunku umiejętność nie mogła usunąć trudności, leżącej w samym przedmiocie, traktowanym przez Kanta, w jego krytycyzmie, opartym na bezwzględnym dualizmie między podmiotem i przedmiotem poznania, między apriorycznymi i aposteriorycznymi czynnikami naszej wiedzy. Stąd to studium Kanta i na podstawie tego przekładu pozostanie zawsze pracą trudną, poważną. Ale przekład Chmielowskiego ułatwił tę pracę znakomicie dla czytelnika polskiego. Toteż można się spodziewać, że się przyczyni skutecznie do oparcia dążności filozoficznych naszych młodszych myślicieli na zasadach prawdziwie krytycznych. A jest to rzeczą bardzo pożądaną wobec dogmatyzmu i sceptycyzmu, grożących sprowadzić nasze życie umysłowe, a szczególniej też konstrukcyjną pracę na polu filozofii, z toru postępu i prawdy, to w jedną, to w drugą stronę.';

columnDivider('cokolwiek');
columnDivider(toDivide);
columnDivider(toDivideTwo);
columnDivider(toDivideThree);